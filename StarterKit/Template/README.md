## Collection/Design (e.g. Basic solids) >> FileName without .scad (e.g. Sphere)

### Title
Name of the Design
If it is part of a collection, please set the collection name, style, or topic name first.
(e.g. BBCool >> Bar stool)
<br>

### Author
Name of Designer
In case of modified versions the name of the modifier, as names of original designer will remain in code
<br>

### Date
Date of upload
<br>

### Images
Table of image, with thumbnail (even linked thumbnails to bigger sized images)
Size is 256 x 256
<br>

| Image 1      | Image 2    |
| :------------- | ---------- |
| ![](images/screenshot_256.jpg) |![](images/ph_lot_256.jpg) |
| Image 3      | Image 4    |
| ![![](images/ph_lot_1024.jpg)](images/ph_lot_256.jpg) | ![](images/ph_lot_256.jpg) |

<br>

### 3d Preview
A link to the stl file in the files folder. Future releases of git will hopefully integrate an STL 3D Viewer, like Github

<script src="files/LoT_Logo.stl"></script>
<br>

### Description
About the design, its design group (e.g. Topic, Style, Type), its use and comments.
<br>


### BOM
Table of Parts, including external parts, that has been used unchanged, or with specific values (e.g. height, width, depth)
<br>

### Notes
Specific notes for production, usage, materials, Machine settings s.o.
<br>

### Modification note
If this design is a modified version, here is the place a general description about changes, that have been made.
<br>

### Link to original
If modified, enter the link to the original version. Please make sure, this is part of the LoT, or otherwise licensed under the same cc-by-sa 4.0 License
<br>
