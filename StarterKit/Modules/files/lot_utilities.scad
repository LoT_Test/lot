/*File Info--------------------------------------------------------------------
File Name: lot_utilities.scad
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the Library of Things, 
    and contains connector modules for CNC and 3D Print designs, 
    e.g. furniture, household items.
Usage: Use "include <filename>" or "use <filename>" to import 
    this module library. 
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------

///* [Array Values] */
//count_x=5;
//count_y=1;
//count_z=2;
//count=[count_x,count_y,count_z];
//distance_x=10;
//distance_y=10;
//distance_z=10;
//distance=[distance_x,distance_y,distance_z];
//radius=60;


//Shape Modules-----------------------------------------------------------

//Triangular Prism module
module triangular_prism(p1,p2,p3,ht,sc)
{
    linear_extrude(height=ht,scale=sc)
    polygon(points=[[0,0],[p1,0],[p2,p3]]);
}

//triangular_prism(80,50,60,10,1);

//Cylinder with Round Edges Modules
module round_edged_cylinder(h,r_cyl,r,fn=0)
{
    if(fn==0)
    {
        translate([0,0,h-r])
        cylinder(r,r=r_cyl-r);
        cylinder(h-r,r=r_cyl);
        translate([0,0,h-r])
        rotate_extrude() 
        translate([r_cyl-r,0,0]) 
        circle(r);
    }
    else
    {
        translate([0,0,h-r])
        cylinder(r,r=r_cyl-r,$fn=fn);
        cylinder(h-r,r=r_cyl,$fn=fn);
        translate([0,0,h-r])
        rotate_extrude($fn=fn) 
        translate([r_cyl-r,0,0]) 
        circle(r,$fn=fn);
    }
}

//Without fn (face numbers), for FreeCad imports
//round_edged_cylinder(5,10,2);
//With fn (face numbers), for direct STL exports
//round_edged_cylinder(5,10,1,20);


//Procedual Modules--------------------------------------------------------------------

module linear_array(count,distance)
{
    for(i=[0:1:count[0]-1])
    {
        for(j=[0:1:count[1]-1])
        {
            for(k=[0:1:count[2]-1])
            {
                translate([distance[0]*i,distance[1]*j,distance[2]*k])
                children();
            }
        }
    }
}

module linear_array_x(count_x,distance_x)
{
    for(i=[0:1:count_x-1])
    {
        translate([distance_x*i,0,0])
        children();
    }
}

module linear_array_y(count_y,distance_y)
{
    for(i=[0:1:count_y-1])
    {
        translate([0,distance_y*i,0])
        children();
    }
}

module linear_array_z(count_z,distance_z)
{
    for(i=[0:1:count_z-1])
    {
        translate([0,0,distance_z*i])
        children();
    }
}

module polar_array_x(count_x,radius)
{
    for(i=[0:360/count_x:360])
    {
        rotate([i,0,0])
        translate([radius,0,0])
        children();
    }
}

module polar_array_y(count_y,radius)
{
    for(i=[0:360/count_y:360])
    {
        rotate([0,i,0])
        translate([radius,0,0])
        children();
    }
}

module polar_array_z(count_z,radius)
{
    for(i=[0:360/count_z:360])
    {
        rotate([0,0,i])
        translate([radius,0,0])
        children();
    }
}

//Examples
//linear_array(count,distance) sphere(5);
//linear_array_z(count_z,distance_z) cube(5,center=true);
//polar_array_z(count_z,radius) cylinder(r=5,h=10,center=true);
